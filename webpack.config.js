const HtmlWebpackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');

module.exports = {
    mode: 'development',
    devServer: {
        port: 80,
        allowedHosts: "all",
    },
    plugins: [
        new ModuleFederationPlugin({
            name: 'products',
            filename: 'remoteEntry.js',
            exposes: {
                './ProductsIndex': './src/index'
            },
            shared: {
                faker: {
                    singleton: true,
                }
            },
        }),
        new HtmlWebpackPlugin({
            template: './public/index.html'
        })
    ]
}