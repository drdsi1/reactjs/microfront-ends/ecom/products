FROM rockylinux:9-minimal

RUN microdnf update -y && microdnf upgrade -y 

RUN microdnf install nodejs -y && npm install -g yarn

WORKDIR /app/products

RUN npm install react react-dom -y && adduser nodejs

RUN echo "192.168.36.166 products" > /etc/hosts

USER nodejs

EXPOSE 80

